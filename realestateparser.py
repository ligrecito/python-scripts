__author__ = 'Javi'
from anonbrowser import *
from BeautifulSoup import BeautifulSoup
import os
import optparse
import re

def printLinks(url):
    ab = anonBrowser()
    ab.anonymize()
    page = ab.open(url)
    html = page.read()
    try:
        print '\n[+] Printing Links From: ' + url
        soup = BeautifulSoup(html)
        links = soup.findAll(name='a')
        for link in links:
            if link.has_key('href'): print link['href']
    except:
        pass


def main():
    """
    This function will connect to realestate.com.au, it will scrape the web based on the search criteria given, and it will yield a summary of the all the match properties.

    """
    parser = optparse.OptionParser('usage%prog -u <target url>')
    parser.add_option('-u', dest='tgtURL', type='string', help='specify target url')
    (options, args) = parser.parse_args()
    url = options.tgtURL
    if url == None:
        print parser.usage
        exit(0)
    else:
        print 'Looking for links to url:' + url
        printLinks(url)

if __name__ == '__main__':
    main()